﻿// Homework_18.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
using namespace std;

class Stack
{
private:
    int* arr;
    int ind = 0;
public:

    int size;

    Stack()
    {
        SetArraySize();
        MakeArray();
    }

    void SetArraySize()
    {
        cout << "Enter the size at array: ";
        cin >> size;
    }

    void MakeArray()
    {
        arr = new int[size];
    }

    void print()
    {
        cout << "Array: ";
        for (int i = 0; i < ind; i++)
        {
            cout << *(arr + i) << ' ';
        }
        cout << '\n';
    }

    int pop()
    {
       
        ind--;
        return *(arr + ind);
    }

    void push(int a)
    {
        *(arr + ind) = a;
        ind++;
    }

    ~Stack()
    {
       cout << "Delete arr";
       delete [] arr;
    }
};

int main()
{
    Stack stack_t;

    cout << "Push " << '\n';
    
    int sizeArr = stack_t.size;

    for (int i = 0; i < sizeArr; i++)
    {
        stack_t.push(i + 1);
    }

    stack_t.print();

    cout << "Pop: " << stack_t.pop() << '\n';

    stack_t.print();

    cout << "Push 9" << '\n';

    stack_t.push(9);

    stack_t.print();
}
